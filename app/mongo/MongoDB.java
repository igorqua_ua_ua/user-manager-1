package mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import models.User;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import play.Logger;
import play.Play;

public final class MongoDB {
    public static MongoClient mongo;
    public static Morphia morphia;
    public static Datastore datastore;

    public static void connect() {
        MongoClientURI mongoURI = new MongoClientURI(readUriFromConfig());
        mongo = null;
        mongo = createMongoClient(mongoURI);
        if (mongo != null) {
            morphia = new Morphia();
            datastore = morphia.createDatastore(mongo, mongoURI.getDatabase());
            mapClasses();
            datastore.ensureIndexes();
            datastore.ensureCaps();
        }
    }

    private static String readUriFromConfig() {
        return Play.application().configuration().getString("mongodb.uri");
    }

    private static MongoClient createMongoClient(MongoClientURI mongoURI) {
        try {
            return new MongoClient(mongoURI);
        } catch (Exception e) {
            Logger.error("Caught Exception in login(): " + e.getMessage(), e);
        }
        return null;
    }

    private static void mapClasses() {
        morphia.map(User.class);
    }


    public static void disconnect() {
        if (mongo != null) {
            morphia = null;
            datastore = null;
            mongo.close();
        }
    }
}

